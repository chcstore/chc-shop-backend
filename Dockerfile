FROM openjdk:11.0

VOLUME /uploads

RUN java --version

COPY chcshop-server.jar .

EXPOSE 8100

CMD ["java", "-jar", "-Dspring.profiles.active=production", "chcshop-server.jar"]
