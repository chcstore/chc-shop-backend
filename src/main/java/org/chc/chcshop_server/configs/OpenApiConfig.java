package org.chc.chcshop_server.configs;

import com.google.common.collect.Lists;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .servers(Lists.newArrayList(
                        new Server().url("http://localhost:8100"),
                        new Server().url("https://nguyenchicuong.website")
                ))
                .info(new Info()
                        .title("CHC Shop Restful APIs")
                        .description("Restful APIs Spring Boot")
                        .contact(new Contact()
                                .email("nguyenchicuong.developer@gmail.com")
                                .name("nguyenchicuong2402")
                                .url("https://nguyenchicuong.website"))
                        .license(new License()
                                .name("Apache 2.0")
                                .url("http://www.apache.org/licenses/LICENSE-2.0.html"))
                        .version("1.0.0"));
    }
}
