package org.chc.chcshop_server.services;

import org.chc.chcshop_server.constants.PrefixId;
import org.chc.chcshop_server.constants.ProviderAuth;
import org.chc.chcshop_server.constants.Roles;
import org.chc.chcshop_server.models.User;
import org.chc.chcshop_server.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class InitDataService implements CommandLineRunner {
    private final UserRepository userRepository;
    private final SequenceGeneratorService sequenceGeneratorService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public InitDataService(UserRepository userRepository,
                           SequenceGeneratorService sequenceGeneratorService,
                           BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.sequenceGeneratorService = sequenceGeneratorService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {
        createDefaultUser("nguyenchicuong2402@gmail.com");
        createDefaultUser("chumin99@hotmail.com");
        createDefaultUser("ngochuyen17011999@gmail.com");
    }

    private void createDefaultUser(String email) {
        if (userRepository.existsUserByEmail(email)) {
            return;
        }

        String password = bCryptPasswordEncoder.encode("@chc12345");

        Set<String> roles = new HashSet<>();
        roles.add(Roles.ADMIN);

        List<String> providers = new ArrayList<>();
        providers.add(ProviderAuth.EMAIL);

        User user = new User(
                sequenceGeneratorService.generateSequence(User.SEQUENCE_NAME, PrefixId.USER),
                email,
                "0945767420",
                "Admin CHC",
                LocalDate.of(1999, 1, 1),
                false,
                "",
                password,
                true,
                providers,
                roles
        );

        userRepository.save(user);
    }
}
