package org.chc.chcshop_server.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
public class MailSenderService {
    private final JavaMailSender javaMailSender;
    private final Environment environment;

    private String profile;

    @Value("${mail.base-url}")
    private String urlWebsite;

    @Autowired
    public MailSenderService(JavaMailSender javaMailSender,
                             Environment environment) {
        this.javaMailSender = javaMailSender;
        this.environment = environment;

        profile = this.environment.getActiveProfiles()[0];
    }

    /**
     * Gửi email đơn giản
     * @param recipient String
     * @param subject String
     * @param message String
     * @return boolean
     */
    public boolean sendSimpleEmail(String recipient, String subject, String message) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

        simpleMailMessage.setTo(recipient);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(message);

        try {
            this.javaMailSender.send(simpleMailMessage);
            return true;
        } catch (MailException e) {
            return false;
        }
    }

//    public boolean sendEmailConfirm(String recipient, int otp) {
//        MimeMessagePreparator mimeMessagePreparator = mimeMessage -> {
//            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "utf-8");
//            messageHelper.setTo(recipient);
//            messageHelper.setSubject("Xác thực tài khoản");
//
//            String urlConfirmEmail = String.format("%s/confirm/%s?code=%d", urlWebsite, recipient, otp);
//
//            String content = mailBuilder.buildMailConfirm(otp, urlConfirmEmail);
//            messageHelper.setText(content, true);
//        };
//
//        try {
//            javaMailSender.send(mimeMessagePreparator);
//            return true;
//        } catch (MailException e) {
//            return false;
//        }
//    }

//    public boolean sendEmailResetPassword(String recipient, int otp) {
//        MimeMessagePreparator mimeMessagePreparator = mimeMessage -> {
//            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "utf-8");
//            messageHelper.setTo(recipient);
//            messageHelper.setSubject("Đặt lại mật khẩu");
//
//            String urlResetPassword = String.format("%s/reset-password/%s?code=%d", urlWebsite, recipient, otp);
//
//            String content = mailBuilder.buildMailResetPassword(otp, urlResetPassword);
//            messageHelper.setText(content, true);
//        };
//
//        try {
//            javaMailSender.send(mimeMessagePreparator);
//            return true;
//        } catch (MailException e) {
//            return false;
//        }
//    }
}
