package org.chc.chcshop_server.controllers;

import org.chc.chcshop_server.helpers.RequestHelper;
import org.chc.chcshop_server.repositories.UserRepository;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/checkout")
public class CheckoutController {
    private final UserRepository userRepository;
    private final RequestHelper requestHelper;

    public CheckoutController(UserRepository userRepository,
                              RequestHelper requestHelper) {
        this.userRepository = userRepository;
        this.requestHelper = requestHelper;
    }
}
