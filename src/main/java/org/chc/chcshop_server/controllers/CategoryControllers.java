package org.chc.chcshop_server.controllers;

import org.chc.chcshop_server.repositories.CategoryRepository;
import org.chc.chcshop_server.repositories.ProductRepository;
import org.chc.chcshop_server.services.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class CategoryControllers {
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    public CategoryControllers(CategoryRepository categoryRepository, ProductRepository productRepository, SequenceGeneratorService sequenceGeneratorService) {
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    @GetMapping("/admin/categories")
    public String categoryManagementPage() {
        return "admin/categories/category-management";
    }
}
