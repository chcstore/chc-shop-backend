package org.chc.chcshop_server.controllers;

import org.chc.chcshop_server.constants.SessionAttributeConst;
import org.chc.chcshop_server.models.Cart;
import org.chc.chcshop_server.models.payloads.ValueResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/v1/sessions")
public class SessionController {
    @GetMapping("/register-session")
    public ResponseEntity<ValueResponse<String>> getSessionId(HttpServletRequest request) {
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute(SessionAttributeConst.CART_SESSION, new ArrayList<Cart>());
        return ResponseEntity.ok(new ValueResponse<>(httpSession.getId()));
    }
}
