package org.chc.chcshop_server.controllers;

import org.chc.chcshop_server.constants.SessionAttributeConst;
import org.chc.chcshop_server.models.Cart;
import org.chc.chcshop_server.models.ProductAttribute;
import org.chc.chcshop_server.models.payloads.ValueResponse;
import org.chc.chcshop_server.repositories.ProductAttributeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping("/")
public class CartController {
    private final ProductAttributeRepository productAttributeRepository;

    public CartController(ProductAttributeRepository productAttributeRepository) {
        this.productAttributeRepository = productAttributeRepository;
    }

    @GetMapping("/cart")
    public String cartPage() {
        return "orders/cart";
    }
}
