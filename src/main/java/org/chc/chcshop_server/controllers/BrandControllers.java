package org.chc.chcshop_server.controllers;

import org.chc.chcshop_server.annotations.RoleAdmin;
import org.chc.chcshop_server.constants.PrefixId;
import org.chc.chcshop_server.exceptions.NotFoundException;
import org.chc.chcshop_server.models.Brand;
import org.chc.chcshop_server.models.payloads.PageableResponse;
import org.chc.chcshop_server.models.payloads.ValueResponse;
import org.chc.chcshop_server.repositories.BrandRepository;
import org.chc.chcshop_server.repositories.ProductRepository;
import org.chc.chcshop_server.services.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class BrandControllers {
    private final BrandRepository brandRepository;
    private final ProductRepository productRepository;
    private final SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    public BrandControllers(BrandRepository brandRepository, ProductRepository productRepository, SequenceGeneratorService sequenceGeneratorService) {
        this.brandRepository = brandRepository;
        this.productRepository = productRepository;
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    @GetMapping("/admin/brands")
    public String brandManagementPage() {
        return "admin/brands/brand-management";
    }
}
