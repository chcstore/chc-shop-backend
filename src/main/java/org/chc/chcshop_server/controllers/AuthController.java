package org.chc.chcshop_server.controllers;

import org.chc.chcshop_server.exceptions.ErrorException;
import org.chc.chcshop_server.exceptions.NotFoundException;
import org.chc.chcshop_server.jwt.JWTAuthenticationService;
import org.chc.chcshop_server.jwt.JwtToken;
import org.chc.chcshop_server.models.User;
import org.chc.chcshop_server.models.payloads.LoginRequest;
import org.chc.chcshop_server.models.payloads.LoginResponse;
import org.chc.chcshop_server.repositories.UserRepository;
import org.chc.chcshop_server.jwt.JwtUserDetailsService;
import org.chc.chcshop_server.services.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;


@Controller
@RequestMapping("/login")
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final JwtToken jwtToken;
    private final JwtUserDetailsService jwtUserDetailsService;
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JWTAuthenticationService jwtAuthenticationService;
    private final SequenceGeneratorService sequenceGeneratorService;
    private final UserController userController;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager,
                          JwtToken jwtToken,
                          JwtUserDetailsService jwtUserDetailsService,
                          UserRepository userRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder,
                          JWTAuthenticationService jwtAuthenticationService,
                          SequenceGeneratorService sequenceGeneratorService,
                          UserController userController) {
        this.authenticationManager = authenticationManager;
        this.jwtToken = jwtToken;
        this.jwtUserDetailsService = jwtUserDetailsService;
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.jwtAuthenticationService = jwtAuthenticationService;
        this.sequenceGeneratorService = sequenceGeneratorService;
        this.userController = userController;
    }

    @GetMapping
    public ModelAndView loginPage() {
        ModelAndView modelAndView = new ModelAndView("accounts/login");
        modelAndView.addObject("loginRequest", new LoginRequest());
        return modelAndView;
    }

    @PostMapping
    public ModelAndView login(@ModelAttribute("loginRequest") @Valid LoginRequest loginRequest) throws Exception {
        ModelAndView modelAndView = new ModelAndView("accounts/login");

        User user = userRepository.findUserByEmail(loginRequest.getEmail())
                .orElse(null);

        if (user == null || !bCryptPasswordEncoder.matches(loginRequest.getPassword(), user.getPassword())) {
            modelAndView.addObject("message", "Tài khoản hoặc mật khẩu không chính xác");
            return modelAndView;
        } else if (!user.isVerified()) {
            modelAndView.addObject("message", "Tài khoản chưa được xác thực");
            return modelAndView;
        }

        authenticate(loginRequest.getEmail(), loginRequest.getPassword());
        final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(loginRequest.getEmail());
        final String token = jwtToken.generateToken(userDetails, loginRequest.isRemember());

        modelAndView.addObject("message", "Đăng nhập thành công");

        return modelAndView;
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}