package org.chc.chcshop_server.controllers;

import org.chc.chcshop_server.annotations.RoleAdmin;
import org.chc.chcshop_server.constants.PrefixId;
import org.chc.chcshop_server.exceptions.NotFoundException;
import org.chc.chcshop_server.models.Brand;
import org.chc.chcshop_server.models.Category;
import org.chc.chcshop_server.models.Product;
import org.chc.chcshop_server.models.Supplier;
import org.chc.chcshop_server.models.payloads.PageableResponse;
import org.chc.chcshop_server.models.payloads.ProductRequest;
import org.chc.chcshop_server.repositories.*;
import org.chc.chcshop_server.services.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class ProductController {
    @GetMapping("/admin/products")
    public String productManagementPage() {
        return "admin/products/product-management";
    }
}
