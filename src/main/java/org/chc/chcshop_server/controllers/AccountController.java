package org.chc.chcshop_server.controllers;

import org.chc.chcshop_server.annotations.RoleAdmin;
import org.chc.chcshop_server.constants.PrefixId;
import org.chc.chcshop_server.constants.ProviderAuth;
import org.chc.chcshop_server.constants.Roles;
import org.chc.chcshop_server.exceptions.ErrorException;
import org.chc.chcshop_server.exceptions.NotFoundException;
import org.chc.chcshop_server.helpers.RequestHelper;
import org.chc.chcshop_server.models.User;
import org.chc.chcshop_server.repositories.UserRepository;
import org.chc.chcshop_server.services.MailSenderService;
import org.chc.chcshop_server.services.OTPService;
import org.chc.chcshop_server.services.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Controller
@RequestMapping("/")
public class AccountController {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final SequenceGeneratorService sequenceGeneratorService;
    private final MailSenderService mailSenderService;
    private final OTPService otpService;
    private final RequestHelper requestHelper;

    @Autowired
    public AccountController(UserRepository userRepository,
                             BCryptPasswordEncoder bCryptPasswordEncoder,
                             SequenceGeneratorService sequenceGeneratorService,
                             MailSenderService mailSenderService,
                             OTPService otpService,
                             RequestHelper requestHelper) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.sequenceGeneratorService = sequenceGeneratorService;
        this.mailSenderService = mailSenderService;
        this.otpService = otpService;
        this.requestHelper = requestHelper;
    }

    @GetMapping("/register")
    public ModelAndView registerPage() {
        ModelAndView modelAndView = new ModelAndView("accounts/register");
        return modelAndView;
    }

    @GetMapping("/forgot-password")
    public ModelAndView forgotPasswordPage() {
        ModelAndView modelAndView = new ModelAndView("accounts/forgot-password");
        return modelAndView;
    }
//
//    @PostMapping("/register")
//    public ResponseEntity<RegisterResponse> register(@Valid @RequestBody RegisterRequest registerRequest) {
//        if (userRepository.existsUserByEmail(registerRequest.getEmail())) {
//            throw new ErrorException("Người dùng đã tồn tại");
//        }
//
//        Set<String> roles = new HashSet<>();
//        roles.add(Roles.USER);
//
//        List<String> providers = new ArrayList<>();
//        providers.add(ProviderAuth.EMAIL);
//
//        String passwordEncode = bCryptPasswordEncoder.encode(registerRequest.getPassword());
//
//        User user = new User(
//                this.sequenceGeneratorService.generateSequence(User.SEQUENCE_NAME, PrefixId.USER),
//                registerRequest.getEmail(),
//                registerRequest.getPhone(),
//                registerRequest.getFullName(),
//                registerRequest.getBirthday(),
//                registerRequest.isFemale(),
//                "",
//                passwordEncode,
//                false,
//                providers,
//                roles
//        );
//
//        User userCreated = this.userRepository.insert(user);
//
//        int otp = otpService.generateOTP(user.getEmail());
//
////        mailSenderService.sendEmailConfirm(userCreated.getEmail(), otp);
//
//        return ResponseEntity.ok(new RegisterResponse(userCreated.getEmail()));
//    }
//
//    @PostMapping("/verification")
//    public ResponseEntity<?> verification(@Valid @RequestBody VerificationRequest verificationRequest) {
//        User user = userRepository.findUserByEmail(verificationRequest.getEmail())
//                .orElseThrow(() -> new NotFoundException("Không tìm thấy người dùng này"));
//
//        if (user.isVerified()) {
//            throw new ErrorException("Tài khoản đã được xác thực");
//        }
//
//        int otp = otpService.generateOTP(user.getEmail());
////        mailSenderService.sendEmailConfirm(verificationRequest.getEmail(), otp);
//        return ResponseEntity.ok(new RegisterResponse(verificationRequest.getEmail()));
//    }
//
//    @PostMapping("/confirm")
//    public ResponseEntity<?> confirm(@Valid @RequestBody OTPRequest otpRequest) {
//        User user = userRepository.findUserByEmail(otpRequest.getUsername())
//                .orElseThrow(() -> new NotFoundException("Không tìm thấy người dùng này"));
//
//        if (user != null && otpRequest.getOtp() == otpService.getOtp(otpRequest.getUsername())) {
//            user.setVerified(true);
//            userRepository.save(user);
//            return ResponseEntity.ok(new SuccessResponse("Xác thực tài khoản thành công"));
//        } else {
//            otpService.clearOTP(otpRequest.getUsername());
//            throw new ErrorException("Mã OTP không đúng hoặc đã hết hạn");
//        }
//    }
//
//    @PostMapping("/forgot-password")
//    public ResponseEntity<?> verification(@Valid @RequestBody ForgotPasswordRequest forgotPasswordRequest) {
//        User user = userRepository.findUserByEmail(forgotPasswordRequest.getEmail())
//                .orElseThrow(() -> new NotFoundException("Không tìm thấy người dùng này"));
//
//        int otp = otpService.generateOTP(user.getEmail());
////        mailSenderService.sendEmailResetPassword(forgotPasswordRequest.getEmail(), otp);
//        return ResponseEntity.ok(new RegisterResponse(forgotPasswordRequest.getEmail()));
//    }
//
//    @PostMapping("/reset-password")
//    public ResponseEntity<?> resetPassword(@Valid @RequestBody ResetPasswordRequest resetPasswordRequest) {
//        User user = userRepository.findUserByEmail(resetPasswordRequest.getEmail())
//                .orElseThrow(() -> new NotFoundException("Không tìm thấy người dùng này"));
//
//        if (resetPasswordRequest.getOtp() == otpService.getOtp(resetPasswordRequest.getEmail())) {
//            String passwordEncode = bCryptPasswordEncoder.encode(resetPasswordRequest.getPassword());
//            user.setPassword(passwordEncode);
//            userRepository.save(user);
//            return ResponseEntity.ok(new SuccessResponse("Cập nhật mật khẩu thành công"));
//        } else {
//            otpService.clearOTP(resetPasswordRequest.getEmail());
//            throw new ErrorException("Đã xảy ra lỗi");
//        }
//    }
//
//    @PostMapping("/change-password")
//    @RoleAdmin
//    public ResponseEntity<?> adminChangePassword(@Valid @RequestBody ChangePasswordRequest changePasswordRequest) {
//        String username = requestHelper.getUsernameFromRequest();
//
//        User user = userRepository.findUserByEmail(username)
//                .orElseThrow(() -> new NotFoundException("Không tìm thấy người dùng này"));
//
//        if (bCryptPasswordEncoder.matches(changePasswordRequest.getOldPassword(), user.getPassword())) {
//            String passwordEncode = bCryptPasswordEncoder.encode(changePasswordRequest.getPassword());
//            user.setPassword(passwordEncode);
//            userRepository.save(user);
//            return ResponseEntity.ok(new SuccessResponse("Cập nhật mật khẩu thành công"));
//        } else {
//            throw new ErrorException("Mật khẩu cũ không chính xác");
//        }
//    }
}