package org.chc.chcshop_server.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @GetMapping
    public String adminPage() {
        return "redirect:/admin/dashboard";
    }

    @GetMapping("/dashboard")
    public String dashboardPage() {
        return "admin/dashboard";
    }
}
