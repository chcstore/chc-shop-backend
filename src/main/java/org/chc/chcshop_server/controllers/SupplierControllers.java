package org.chc.chcshop_server.controllers;

import org.chc.chcshop_server.constants.PrefixId;
import org.chc.chcshop_server.models.Supplier;
import org.chc.chcshop_server.repositories.ProductRepository;
import org.chc.chcshop_server.repositories.SupplierRepository;
import org.chc.chcshop_server.services.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class SupplierControllers {
    private final SupplierRepository supplierRepository;
    private final ProductRepository productRepository;
    private final SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    public SupplierControllers(SupplierRepository supplierRepository,
                               ProductRepository productRepository,
                               SequenceGeneratorService sequenceGeneratorService) {
        this.supplierRepository = supplierRepository;
        this.productRepository = productRepository;
        this.sequenceGeneratorService = sequenceGeneratorService;
    }


    @GetMapping("/admin/suppliers")
    public ModelAndView supplierManagementPage(@RequestParam(required = false) String q) {
        // lấy danh sách nhà cung cấp
        List<Supplier> listSupplier = new ArrayList<>();
        ModelAndView modelAndView = new ModelAndView("admin/suppliers/supplier-management");

        if (q != null && !q.isEmpty()) {
            TextCriteria criteria = TextCriteria.forDefaultLanguage().matchingAny(q);
            listSupplier = supplierRepository.findAllBy(criteria);
            modelAndView.addObject("q", q);
        } else {
            listSupplier = supplierRepository.findAll();
        }

        modelAndView.addObject("listSupplier", listSupplier);

        return modelAndView;
    }

    @GetMapping("/admin/supplier")
    public ModelAndView supplierEditorPage(@RequestParam(required = false) String id) {
        /*
        Kiểm tra path variable có id không
        Nếu có thì lấy data từ db và trả về
        Nếu không thì tạo mới
         */
        Supplier supplier = new Supplier();

        if (id != null && !id.isEmpty()) {
            supplier = supplierRepository.findById(id)
                    .orElse(new Supplier());
        }

        ModelAndView modelAndView = new ModelAndView("admin/suppliers/supplier-editor");
        modelAndView.addObject("supplier", supplier);

        return modelAndView;
    }

    @PostMapping("/admin/supplier")
    public ModelAndView createOrUpdate(@ModelAttribute("supplier") @Valid Supplier supplier) {
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/suppliers");
        /*
          Kiểm tra id có tồn tại không
          Nếu có thì cập nhật
          Nếu không có thì tạo mới
         */
        if (supplier.getSupplierId().trim().isEmpty()) {
            // Generate id mới
            supplier.setSupplierId(this.sequenceGeneratorService
                    .generateSequence(Supplier.SEQUENCE_NAME, PrefixId.SUPPLIER));

            // lưu vào db
            supplierRepository.insert(supplier);
        } else {
            // kiểm tra supplier đó có tồn tại trong db không
            Optional<Supplier> supplierFindOptional = supplierRepository.findById(supplier.getSupplierId());
            // cập nhật
            if (supplierFindOptional.isPresent()) {
                Supplier supplierFind = supplierFindOptional.get();
                supplierFind.setSupplierName(supplier.getSupplierName());
                supplierFind.setDescription(supplier.getDescription());
                supplierFind.setContactPerson(supplier.getContactPerson());
                supplierFind.setPhone(supplier.getPhone());
                supplierFind.setAddress(supplier.getAddress());
                supplierRepository.save(supplierFind);
            } else {
                modelAndView.addObject("supplier", supplier);
                modelAndView.setViewName("admin/suppliers/supplier-editor");
            }
        }

        return modelAndView;
    }
}
