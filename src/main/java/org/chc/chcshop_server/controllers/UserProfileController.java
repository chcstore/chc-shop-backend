package org.chc.chcshop_server.controllers;

import org.chc.chcshop_server.annotations.RoleUser;
import org.chc.chcshop_server.exceptions.NotFoundException;
import org.chc.chcshop_server.helpers.RequestHelper;
import org.chc.chcshop_server.models.User;
import org.chc.chcshop_server.models.payloads.UpdateProfileRequest;
import org.chc.chcshop_server.repositories.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/me")
public class UserProfileController {
    private final UserRepository userRepository;
    private final RequestHelper requestHelper;

    public UserProfileController(UserRepository userRepository,
                                 RequestHelper requestHelper) {
        this.userRepository = userRepository;
        this.requestHelper = requestHelper;
    }

    @GetMapping
    @RoleUser
    public ResponseEntity<User> getProfile() {
        String username = requestHelper.getUsernameFromRequest();
        User user = userRepository.findUserByEmail(username)
                .orElseThrow(() -> new NotFoundException("Không tìm thấy người dùng này"));

        return ResponseEntity.ok(user);
    }

    @PutMapping
    @RoleUser
    public ResponseEntity<User> updateProfile(@Valid @RequestBody UpdateProfileRequest profile) {
        String username = requestHelper.getUsernameFromRequest();
        User userUpdate = userRepository.findUserByEmail(username)
                .map(user -> {
                    user.setPhone(profile.getPhone());
                    user.setFullName(profile.getFullName());
                    user.setLinkAvatar(profile.getLinkAvatar());
                    user.setAddress(profile.getAddress());
                    user.setFemale(profile.isFemale());
                    user.setBirthday(profile.getBirthday());

                    return user;
                }).orElseThrow(() -> new NotFoundException("Không tìm thấy người dùng này"));

        return ResponseEntity.ok(userRepository.save(userUpdate));
    }
}
