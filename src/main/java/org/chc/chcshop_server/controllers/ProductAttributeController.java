package org.chc.chcshop_server.controllers;

import org.chc.chcshop_server.annotations.RoleAdmin;
import org.chc.chcshop_server.constants.PrefixId;
import org.chc.chcshop_server.exceptions.ErrorException;
import org.chc.chcshop_server.exceptions.NotFoundException;
import org.chc.chcshop_server.models.*;
import org.chc.chcshop_server.models.payloads.PageableResponse;
import org.chc.chcshop_server.repositories.ProductAttributeRepository;
import org.chc.chcshop_server.repositories.ProductRepository;
import org.chc.chcshop_server.services.SequenceGeneratorService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/products")
public class ProductAttributeController {
    private final ProductRepository productRepository;
    private final ProductAttributeRepository productAttributeRepository;
    private final SequenceGeneratorService sequenceGeneratorService;

    public ProductAttributeController(ProductRepository productRepository,
                                      ProductAttributeRepository productAttributeRepository,
                                      SequenceGeneratorService sequenceGeneratorService) {
        this.productRepository = productRepository;
        this.productAttributeRepository = productAttributeRepository;
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    @GetMapping("/{productId}/{productAttributeId}")
    public ResponseEntity<ProductAttribute> getById(@PathVariable String productId,
                                                    @PathVariable String productAttributeId) {
        productRepository.findById(productId).orElseThrow(() -> new NotFoundException("Không tìm thấy sản phẩm"));

        ProductAttribute productAttribute = productAttributeRepository.findById(productAttributeId)
                .orElseThrow(() -> new NotFoundException("Không tìm thấy sản phẩm con"));
        return ResponseEntity.ok(productAttribute);
    }

    @GetMapping("/{productId}/attributes/list")
    public ResponseEntity<PageableResponse> get(@PathVariable String productId,
                                                @RequestParam(required = false) Integer page,
                                                @RequestParam(required = false) Integer pageSize) {
        List<ProductAttribute> productAttributeList = new ArrayList<>();
        if (page != null && pageSize != null) {
            page = Math.max(page - 1, 0);
            pageSize = Math.max(pageSize, 1);
            Pageable pageable = PageRequest.of(page, pageSize);
            productAttributeList = productAttributeRepository.findAllByProduct_ProductId(productId, pageable);
        } else {
            productAttributeList = productAttributeRepository.findAllByProduct_ProductId(productId);
        }

        long total = productAttributeRepository.countAllByProduct_ProductId(productId);

        return ResponseEntity.ok(new PageableResponse(productAttributeList, total));
    }

    @PostMapping("/{productId}/attributes")
    @RoleAdmin
    public ResponseEntity<ProductAttribute> create(@PathVariable String productId,
                                            @Valid @RequestBody ProductAttribute productAttribute) {
        productAttribute.setProductAttributeId(sequenceGeneratorService
                .generateSequence(ProductAttribute.SEQUENCE_NAME, PrefixId.PRODUCT_ATTRIBUTE));

        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new NotFoundException("Không tìm thấy sản phẩm"));
        productAttribute.setProduct(product);

        ProductAttribute productAttributeCreate = productAttributeRepository.insert(productAttribute);

        List<ProductAttribute> productAttributeList = null;
        try {
            productAttributeList = product.getProductAttributeList();
            productAttributeList.add(productAttributeCreate);
        } catch (Exception e) {
            productAttributeList = new ArrayList<>();
            productAttributeList.add(productAttributeCreate);
        } finally {
            product.setProductAttributeList(productAttributeList);
            productRepository.save(product);
        }

        return ResponseEntity.ok(productAttributeCreate);
    }

    @PutMapping("/{productId}/attributes/{productAttributeId}")
    @RoleAdmin
    public ResponseEntity<ProductAttribute> update(@PathVariable String productId,
                                                   @PathVariable String productAttributeId,
                                                   @Valid @RequestBody ProductAttribute productAttribute) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new NotFoundException("Không tìm thấy sản phẩm"));

        ProductAttribute productAttributeResult = productAttributeRepository.findById(productAttributeId)
                .map(productAttributeFind -> {
                    productAttributeFind.setColor(productAttribute.getColor());
                    productAttributeFind.setPrice(productAttribute.getPrice());
                    productAttributeFind.setQuantity(productAttribute.getQuantity());
                    productAttributeFind.setSku(productAttribute.getSku());
                    productAttributeFind.setDiscount(productAttribute.getDiscount());

                    ProductAttribute productAttributeUpdate = productAttributeRepository.save(productAttributeFind);

                    try {
                        List<ProductAttribute> productAttributeList = product.getProductAttributeList();
                        productAttributeList.remove(productAttributeFind);
                        productAttributeList.add(productAttributeUpdate);
                        product.setProductAttributeList(productAttributeList);
                        productRepository.save(product);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }

                    return productAttributeUpdate;
                }).orElseThrow(() -> new NotFoundException("Không tìm thấy sản phẩm con"));

        return ResponseEntity.ok(productAttributeResult);
    }

    @DeleteMapping("/{productId}/attributes/{productAttributeId}")
    @RoleAdmin
    public ResponseEntity<ProductAttribute> delete(@PathVariable String productId,
                                                   @PathVariable String productAttributeId) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new NotFoundException("Không tìm thấy sản phẩm"));

        ProductAttribute productAttribute = productAttributeRepository.findById(productAttributeId)
                .map(productAttributeFind -> {
                    productAttributeRepository.delete(productAttributeFind);

                    List<ProductAttribute> productAttributeList = null;
                    try {
                        productAttributeList = product.getProductAttributeList();
                        productAttributeList.remove(productAttributeFind);
                    } catch (Exception e) {
                        throw new ErrorException("Sản phẩm không tồn tại");
                    } finally {
                        product.setProductAttributeList(productAttributeList);
                        productRepository.save(product);
                    }

                    return productAttributeFind;
                }).orElseThrow(() -> new NotFoundException("Không tìm thấy sản phẩm con"));
        return ResponseEntity.ok(productAttribute);
    }
}
