package org.chc.chcshop_server.controllers;

import org.chc.chcshop_server.exceptions.ErrorException;
import org.chc.chcshop_server.models.payloads.SuccessResponse;
import org.chc.chcshop_server.services.MailSenderService;
import org.chc.chcshop_server.services.OTPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/otp-code")
public class OTPController {
    private final OTPService otpService;
    private final MailSenderService mailSenderService;

    @Autowired
    public OTPController(OTPService otpService, MailSenderService mailSenderService) {
        this.otpService = otpService;
        this.mailSenderService = mailSenderService;
    }

    @GetMapping("/generate")
    public ResponseEntity<?> generate() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        int otp = otpService.generateOTP(username);
        mailSenderService.sendSimpleEmail(username, "OTP", String.valueOf(otp));
        return ResponseEntity.ok(new SuccessResponse("Gửi mã OTP thành công"));
    }

    @PostMapping("/validate")
    public ResponseEntity<?> validate(@RequestBody int otp) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();

        if(otp >= 0){
            int serverOtp = otpService.getOtp(username);
            if(serverOtp > 0){
                if(otp == serverOtp){
                    otpService.clearOTP(username);
                    throw new ErrorException("Mã OTP không đúng hoặc đã hết hạn");
                }else{
                    return ResponseEntity.ok(new SuccessResponse("Xác thực OTP thành công"));
                }
            }else {
                throw new ErrorException("Mã OTP không đúng hoặc đã hết hạn");
            }
        }else {
            throw new ErrorException("Mã OTP không đúng hoặc đã hết hạn");
        }
    }
}
