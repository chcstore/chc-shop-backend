package org.chc.chcshop_server.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class OrderController {
    @GetMapping("/admin/orders")
    public String orderManagementPage() {
        return "admin/orders/order-management";
    }

    @GetMapping("/checkout")
    public ModelAndView checkoutPage() {
        ModelAndView modelAndView = new ModelAndView("orders/checkout");
        return modelAndView;
    }
}
