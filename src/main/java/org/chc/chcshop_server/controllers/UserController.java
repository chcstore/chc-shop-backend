package org.chc.chcshop_server.controllers;

import org.chc.chcshop_server.annotations.RoleAdmin;
import org.chc.chcshop_server.constants.PrefixId;
import org.chc.chcshop_server.constants.Roles;
import org.chc.chcshop_server.exceptions.ErrorException;
import org.chc.chcshop_server.exceptions.NotFoundException;
import org.chc.chcshop_server.models.User;
import org.chc.chcshop_server.models.payloads.PageableResponse;
import org.chc.chcshop_server.models.payloads.UserRequest;
import org.chc.chcshop_server.repositories.UserRepository;
import org.chc.chcshop_server.services.MailSenderService;
import org.chc.chcshop_server.services.OTPService;
import org.chc.chcshop_server.services.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {
    private final UserRepository userRepository;
    private final SequenceGeneratorService sequenceGeneratorService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final OTPService otpService;
    private final MailSenderService mailSenderService;
    private final MongoTemplate mongoTemplate;

    @Autowired
    public UserController(UserRepository userRepository,
                          SequenceGeneratorService sequenceGeneratorService,
                          BCryptPasswordEncoder bCryptPasswordEncoder,
                          OTPService otpService,
                          MailSenderService mailSenderService,
                          MongoTemplate mongoTemplate) {
        this.userRepository = userRepository;
        this.sequenceGeneratorService = sequenceGeneratorService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.otpService = otpService;
        this.mailSenderService = mailSenderService;
        this.mongoTemplate = mongoTemplate;
    }

    @GetMapping("/list")
    @RoleAdmin
    public ResponseEntity<PageableResponse> findAll(@RequestParam(required = false) Integer page,
                                                    @RequestParam(required = false) Integer size) {
        List<User> users = new ArrayList<>();
        if (page != null && size != null) {
            page = Math.max(page - 1, 0);
            size = Math.max(size, 1);
            Pageable pageable = PageRequest.of(page, size);
            users = userRepository.findAll(pageable).getContent();
        } else {
            users = userRepository.findAll();
        }

        long total = userRepository.count();

        return ResponseEntity.ok(new PageableResponse(users, total));
    }

    @GetMapping("/{userId}")
    @RoleAdmin
    public ResponseEntity<User> findByUserId(@PathVariable String userId) {
        return ResponseEntity.ok(userRepository.findById(userId)
                .map(user -> ResponseEntity.ok(user).getBody())
                .orElseThrow(() -> new NotFoundException("Không tìm thấy người dùng")));
    }

    @PostMapping
    @RoleAdmin
    public ResponseEntity<User> create(@Valid @RequestBody UserRequest user) {
        if (userRepository.existsById(user.getEmail())) {
            throw new ErrorException("Người dùng đã tồn tại");
        }

        Set<String> roles = new HashSet<>();
        AtomicBoolean isAdmin = new AtomicBoolean(false);

        user.getRoles().forEach(role -> {
            if ("ADMIN".equals(role)) {
                roles.add(Roles.ADMIN);
                isAdmin.set(true);
            }
            roles.add(Roles.USER);
        });

        String passwordEncode = bCryptPasswordEncoder.encode(user.getPassword());

        user.setUid(this.sequenceGeneratorService
                .generateSequence(User.SEQUENCE_NAME, PrefixId.USER));
        user.setPassword(passwordEncode);
        user.setRoles(roles);
        User userCreated = userRepository.save(user);

        if (!user.isVerified()) {
            int otp = otpService.generateOTP(user.getEmail());
//            mailSenderService.sendEmailConfirm(userCreated.getEmail(), otp);
        }

        return ResponseEntity.ok(userCreated);
    }

    @PutMapping("/{userId}")
    @RoleAdmin
    public ResponseEntity<User> update(@PathVariable String userId,
                                       @Valid @RequestBody UserRequest user) {
        User userUpdate = userRepository.findById(userId)
                .map(userFind -> {
                    Set<String> roles = new HashSet<>();
                    AtomicBoolean isAdmin = new AtomicBoolean(false);

                    user.getRoles().forEach(role -> {
                        if ("ADMIN".equals(role)) {
                            roles.add(Roles.ADMIN);
                            isAdmin.set(true);
                        }
                        roles.add(Roles.USER);
                    });

                    String passwordEncode = bCryptPasswordEncoder.encode(user.getPassword());

                    if (!user.getPassword().isEmpty()) {
                        userFind.setPassword(passwordEncode);
                    }

                    userFind.setFullName(user.getFullName());
                    userFind.setBirthday(user.getBirthday());
                    userFind.setFemale(user.isFemale());
                    userFind.setLinkAvatar(user.getLinkAvatar());
                    userFind.setAddress(user.getAddress());
                    userFind.setVerified(user.isVerified());
                    userFind.setRoles(roles);

                    if (!userFind.isVerified()) {
                        int otp = otpService.generateOTP(user.getEmail());
//                        mailSenderService.sendEmailConfirm(userFind.getEmail(), otp);
                    }

                    return user;
                })
                .orElseThrow(() -> new NotFoundException("Không tìm thấy người dùng"));
        return ResponseEntity.ok(userUpdate);
    }

    @DeleteMapping("/{userId}")
    @RoleAdmin
    public ResponseEntity<User> delete(@PathVariable String userId) {
        User userFind = userRepository.findById(userId)
                .map(user -> {
                    userRepository.delete(user);
                    return user;
                })
                .orElseThrow(() -> new ErrorException("Người dùng không tồn tại"));
        return ResponseEntity.ok(userFind);
    }
}
