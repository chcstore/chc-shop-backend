package org.chc.chcshop_server.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.context.annotation.Scope;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@RequiredArgsConstructor
@Component
@Scope("session")
public class Cart implements Serializable {
    @NotEmpty
    @NonNull
    private String productId;

    @NotEmpty
    @NonNull
    private String productAttributeId;

    @EqualsAndHashCode.Exclude
    @NotNull
    @NonNull
    private int quantity;

    @EqualsAndHashCode.Exclude
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private double totalPrice;
}
