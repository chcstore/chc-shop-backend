package org.chc.chcshop_server.models.changes;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import org.chc.chcshop_server.models.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Date;

@ChangeLog
public class DatabaseChangelog {
//    @ChangeSet(order = "001", id = "updateVersion", author = "nguyenchicuong2402")
//    public void changeVersion(MongoTemplate mongoTemplate) {
//        Query query = Query.query(Criteria.where("version").is(null)
//                .orOperator(Criteria.where("version").exists(false)));
//
//        Update update = new Update();
//        update.set("version", 0L);
//
//        mongoTemplate.updateMulti(query, update, Ward.class);
//        mongoTemplate.updateMulti(query, update, District.class);
//        mongoTemplate.updateMulti(query, update, City.class);
//        mongoTemplate.updateMulti(query, update, AddressBook.class);
//        mongoTemplate.updateMulti(query, update, Brand.class);
//        mongoTemplate.updateMulti(query, update, Category.class);
//        mongoTemplate.updateMulti(query, update, Product.class);
//        mongoTemplate.updateMulti(query, update, ProductAttribute.class);
//        mongoTemplate.updateMulti(query, update, Supplier.class);
//        mongoTemplate.updateMulti(query, update, User.class);
//    }
//
//    @ChangeSet(order = "002", id = "updateCreatedAt", author = "nguyenchicuong2402")
//    public void changeCreatedAt(MongoTemplate mongoTemplate) {
//        Query query = Query.query(Criteria.where("createdAt").is(null)
//                .orOperator(Criteria.where("createdAt").exists(false)));
//
//        Date now = new Date();
//
//        Update update = new Update();
//        update.set("createdAt", now);
//        update.set("updatedAt", now);
//
//        mongoTemplate.updateMulti(query, update, Ward.class);
//        mongoTemplate.updateMulti(query, update, District.class);
//        mongoTemplate.updateMulti(query, update, City.class);
//        mongoTemplate.updateMulti(query, update, AddressBook.class);
//        mongoTemplate.updateMulti(query, update, Brand.class);
//        mongoTemplate.updateMulti(query, update, Category.class);
//        mongoTemplate.updateMulti(query, update, Product.class);
//        mongoTemplate.updateMulti(query, update, ProductAttribute.class);
//        mongoTemplate.updateMulti(query, update, Supplier.class);
//        mongoTemplate.updateMulti(query, update, User.class);
//    }
}
