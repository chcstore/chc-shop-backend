package org.chc.chcshop_server.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@Document(collection = "users")
@TypeAlias(value = "users")
public class User extends AuditModel {
    @Transient
    public static final String SEQUENCE_NAME = "users_sequence";

    @MongoId
    @NonNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String uid;

    @NotEmpty
    @NonNull
    private String email;

    @NotEmpty
    @NonNull
    private String phone;

    @EqualsAndHashCode.Exclude
    @NotEmpty
    @NonNull
    private String fullName;

    @EqualsAndHashCode.Exclude
    @NotNull
    @NonNull
    private LocalDate birthday;

    @EqualsAndHashCode.Exclude
    @NotNull
    @NonNull
    private boolean female;

    @EqualsAndHashCode.Exclude
    @NonNull
    private String linkAvatar;

    @EqualsAndHashCode.Exclude
    private String address;

    @EqualsAndHashCode.Exclude
    @NonNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @EqualsAndHashCode.Exclude
    @NonNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private boolean isVerified;

    @EqualsAndHashCode.Exclude
    @NonNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<String> providers;

    @EqualsAndHashCode.Exclude
    @NonNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Set<String> roles;

    public void addProvider(String provider) {
        if (providers == null) {
            providers = new ArrayList<>();
        }

        boolean isExist = false;
        for (String item: providers) {
            isExist = item.equalsIgnoreCase(provider);
            if (isExist) break;
        }

        if (!isExist) {
            providers.add(provider);
        }
    }
}
