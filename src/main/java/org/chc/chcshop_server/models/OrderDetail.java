package org.chc.chcshop_server.models;

import lombok.*;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class OrderDetail extends AuditModel {
    @NonNull
    private int quantity;

    @NonNull
    private double unitPrice;

    @NonNull
    private double discount;

    @DBRef
    private ProductAttribute productAttribute;
}
