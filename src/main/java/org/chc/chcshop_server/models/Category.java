package org.chc.chcshop_server.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@RequiredArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Document(collection = "categories")
@TypeAlias(value = "categories")
public class Category extends AuditModel {
    @Transient
    public static final String SEQUENCE_NAME = "categories_sequence";

    @TextIndexed
    @MongoId
    @NonNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String categoryId;

    @TextIndexed
    @EqualsAndHashCode.Exclude
    @NotEmpty
    @NonNull
    private String categoryName;

    @TextIndexed(weight = 2)
    @EqualsAndHashCode.Exclude
    @NonNull
    private String description;
}
