package org.chc.chcshop_server.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@RequiredArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Document(collection = "brands")
@TypeAlias(value = "brands")
public class Brand extends AuditModel {
    @Transient
    public static final String SEQUENCE_NAME = "brands_sequence";

    @TextIndexed
    @MongoId
    @NonNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String brandId;

    @TextIndexed
    @EqualsAndHashCode.Exclude
    @NotEmpty
    @NonNull
    private String brandName;

    @TextIndexed(weight = 2)
    @EqualsAndHashCode.Exclude
    @NonNull
    private String description;
}
