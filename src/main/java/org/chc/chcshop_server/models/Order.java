package org.chc.chcshop_server.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import org.chc.chcshop_server.constants.OrderStatus;
import org.chc.chcshop_server.helpers.LocalDateTimeDeserializer;
import org.chc.chcshop_server.helpers.LocalDateTimeSerializer;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.persistence.ElementCollection;
import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@Document(collection = "orders")
@TypeAlias(value = "orders")
public class Order extends AuditModel {
    @Transient
    public static final String SEQUENCE_NAME = "orders_sequence";

    @MongoId
    private String orderId;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime shippedDate;

    private OrderStatus status;

    private double total;

    private double shippingFee;

    private float weight;

    private String note;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime orderDate;

    private String address;

    @DBRef
    private User user;

    @ElementCollection
    private Set<OrderDetail> orderDetails;
}
