package org.chc.chcshop_server.models.payloads;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.chc.chcshop_server.models.User;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class UserRequest extends User {
    @NotEmpty
    private String email;

    @NotEmpty
    private String phone;

    @NotEmpty
    private String fullName;

    @NotNull
    private LocalDate birthday;

    @NotNull
    private boolean female;

    private String linkAvatar;

    private String address;

    @NotEmpty
    private String wardId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private boolean isVerified;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Set<String> roles;
}
