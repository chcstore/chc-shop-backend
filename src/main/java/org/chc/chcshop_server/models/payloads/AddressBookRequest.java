package org.chc.chcshop_server.models.payloads;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class AddressBookRequest implements Serializable {
    @NotEmpty
    private String fullName;

    @NotEmpty
    private String address;

    @NotEmpty
    private String wardId;

    @NotEmpty
    private String phone;
}
