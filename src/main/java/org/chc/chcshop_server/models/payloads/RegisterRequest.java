package org.chc.chcshop_server.models.payloads;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class RegisterRequest implements Serializable {
    @NotEmpty
    private String email;

    @NotEmpty
    private String phone;

    @NotEmpty
    private String password;

    @NotEmpty
    private String fullName;

    @NotNull
    private boolean female;

    @NotNull
    private LocalDate birthday;
}
