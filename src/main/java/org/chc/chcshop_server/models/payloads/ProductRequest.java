package org.chc.chcshop_server.models.payloads;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest implements Serializable {

    @NotEmpty
    private String productName;

    private String description;

    private List<String> images;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int quantity;

    @NotNull
    private int warrantyPeriod;

    @NotEmpty
    private String origin;

    @NotNull
    private float weight;

    @NotNull
    private float length;

    @NotNull
    private float width;

    @NotNull
    private float height;

    @NotNull
    private boolean published;

    @NotEmpty
    private String brandId;

    @NotEmpty
    private String supplierId;

    @NotEmpty
    private String categoryId;
}
