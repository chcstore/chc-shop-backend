package org.chc.chcshop_server.models.payloads;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class SupplierRequest implements Serializable {

    @NotEmpty
    private String supplierName;

    private String description;

    @NotEmpty
    private String contactPerson;

    @NotEmpty
    private String phone;

    private String linkAvatar;

    private String address;

    @NotEmpty
    private String wardId;
}
