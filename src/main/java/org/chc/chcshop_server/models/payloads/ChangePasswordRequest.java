package org.chc.chcshop_server.models.payloads;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ChangePasswordRequest implements Serializable {
    @NotEmpty
    private String oldPassword;

    @NotEmpty
    private String password;
}
