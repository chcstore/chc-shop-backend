package org.chc.chcshop_server.models.payloads;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class SuccessResponse implements Serializable {
    private String message;
}
