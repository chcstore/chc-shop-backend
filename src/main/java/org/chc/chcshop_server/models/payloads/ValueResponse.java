package org.chc.chcshop_server.models.payloads;

import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ValueResponse<T> {
    private T value;
}
