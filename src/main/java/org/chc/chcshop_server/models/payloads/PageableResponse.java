package org.chc.chcshop_server.models.payloads;

import lombok.*;

import java.io.Serializable;
import java.util.Collection;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class PageableResponse implements Serializable {
    Collection<?> list;
    long total;
}
