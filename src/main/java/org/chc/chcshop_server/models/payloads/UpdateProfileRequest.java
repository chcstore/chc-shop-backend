package org.chc.chcshop_server.models.payloads;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class UpdateProfileRequest implements Serializable {
    @NotEmpty
    private String phone;

    @NotEmpty
    private String fullName;

    private String linkAvatar;

    @NotEmpty
    private String address;

    @NotEmpty
    private String wardId;

    @NotNull
    private boolean female;

    @NotNull
    private LocalDate birthday;
}
