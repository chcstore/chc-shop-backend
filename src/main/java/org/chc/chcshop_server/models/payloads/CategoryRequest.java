package org.chc.chcshop_server.models.payloads;

import lombok.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class CategoryRequest implements Serializable {
    @EqualsAndHashCode.Exclude
    @NotEmpty
    private String categoryName;

    @EqualsAndHashCode.Exclude
    private String description;

    @EqualsAndHashCode.Exclude
    private String categoryParentId;
}

