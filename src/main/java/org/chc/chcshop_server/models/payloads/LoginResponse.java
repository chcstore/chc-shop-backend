package org.chc.chcshop_server.models.payloads;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class LoginResponse implements Serializable {
    private String token;
}
