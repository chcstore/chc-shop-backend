package org.chc.chcshop_server.models.payloads;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ResetPasswordRequest implements Serializable {
    @NotEmpty
    private String email;

    @NotNull
    private int otp;

    @NotEmpty
    private String password;
}
