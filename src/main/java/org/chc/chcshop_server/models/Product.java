package org.chc.chcshop_server.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.persistence.ElementCollection;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@RequiredArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Document(collection = "products")
@TypeAlias(value = "products")
public class Product extends AuditModel {
    @Transient
    public static final String SEQUENCE_NAME = "products_sequence";

    @TextIndexed
    @MongoId
    @NonNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String productId;

    @TextIndexed
    @EqualsAndHashCode.Exclude
    @NotEmpty
    @NonNull
    private String productName;

    @TextIndexed(weight = 2)
    @EqualsAndHashCode.Exclude
    @NonNull
    private String description;

    @EqualsAndHashCode.Exclude
    @NonNull
    private List<String> images;

    @EqualsAndHashCode.Exclude
    @NonNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int quantity;

    @EqualsAndHashCode.Exclude
    @NotNull
    @NonNull
    private int warrantyPeriod;

    @TextIndexed(weight = 2)
    @EqualsAndHashCode.Exclude
    @NotEmpty
    @NonNull
    private String origin;

    @EqualsAndHashCode.Exclude
    @NotNull
    @NonNull
    private float weight;

    @EqualsAndHashCode.Exclude
    @NotNull
    @NonNull
    private float length;

    @EqualsAndHashCode.Exclude
    @NotNull
    @NonNull
    private float width;

    @EqualsAndHashCode.Exclude
    @NotNull
    @NonNull
    private float height;


    @EqualsAndHashCode.Exclude
    @NotNull
    @NonNull
    private boolean published;

    @EqualsAndHashCode.Exclude
    @NonNull
    @DBRef
    private Brand brand;

    @EqualsAndHashCode.Exclude
    @NonNull
    @DBRef
    private Supplier supplier;

    @EqualsAndHashCode.Exclude
    @NonNull
    @DBRef
    private Category category;

    @EqualsAndHashCode.Exclude
    @NonNull
    @DBRef
    @ElementCollection
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<ProductAttribute> productAttributeList;

    @EqualsAndHashCode.Exclude
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private long view;
}
