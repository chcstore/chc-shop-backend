package org.chc.chcshop_server.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Document(collection = "product_attributes")
@TypeAlias(value = "product_attributes")
public class ProductAttribute {
    @Transient
    public static final String SEQUENCE_NAME = "product_attributes_sequence";

    @MongoId
    @NonNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String productAttributeId;

    @NotEmpty
    @NonNull
    private String color;

    @EqualsAndHashCode.Exclude
    @NotNull
    @NonNull
    private double price;

    @EqualsAndHashCode.Exclude
    @NotNull
    @NonNull
    private int quantity;

    @NotEmpty
    @NonNull
    private String sku;

    @EqualsAndHashCode.Exclude
    @NotNull
    @NonNull
    private float discount;

    @EqualsAndHashCode.Exclude
    @DBRef
    @NonNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Product product;
}
