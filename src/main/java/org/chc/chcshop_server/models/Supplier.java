package org.chc.chcshop_server.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@RequiredArgsConstructor
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@Document(collection = "suppliers")
@TypeAlias(value = "suppliers")
public class Supplier extends AuditModel {
    @Transient
    public static final String SEQUENCE_NAME = "suppliers_sequence";

    @MongoId
    @TextIndexed
    @NonNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String supplierId;

    @TextIndexed
    @EqualsAndHashCode.Exclude
    @NotEmpty(message = "Vui lòng nhập tên nhà cung cấp")
    @NonNull
    private String supplierName;

    @TextIndexed
    @EqualsAndHashCode.Exclude
    @NonNull
    private String description;

    @TextIndexed
    @EqualsAndHashCode.Exclude
    @NotEmpty(message = "Vui lòng nhập tên người liên hệ")
    @NonNull
    private String contactPerson;

    @TextIndexed
    @EqualsAndHashCode.Exclude
    @NotEmpty(message = "Vui lòng nhập số điện thoại")
    @NonNull
    private String phone;

    @TextIndexed
    @EqualsAndHashCode.Exclude
    private String address;
}
