package org.chc.chcshop_server.models;

import lombok.*;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@AllArgsConstructor
@Document(collection = "sequences")
@TypeAlias(value = "sequences")
public class DatabaseSequence extends AuditModel {
    @MongoId
    private String sequenceId;

    private long sequence;
}
