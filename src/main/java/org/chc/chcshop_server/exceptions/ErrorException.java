package org.chc.chcshop_server.exceptions;

public class ErrorException extends RuntimeException {
    public ErrorException(String message) {
        super(message);
    }
}
