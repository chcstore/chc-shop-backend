package org.chc.chcshop_server.exceptions;

public class ValidateException extends Exception {
    public ValidateException() {
        super();
    }

    public ValidateException(String message) {
        super(message);
    }
}
