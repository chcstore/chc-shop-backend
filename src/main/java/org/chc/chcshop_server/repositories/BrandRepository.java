package org.chc.chcshop_server.repositories;

import org.chc.chcshop_server.models.Brand;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BrandRepository extends MongoRepository<Brand, String> {
}
