package org.chc.chcshop_server.repositories;

import org.chc.chcshop_server.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
    Optional<User> findUserByEmail(String email);
    boolean existsUserByEmail(String email);
}
