package org.chc.chcshop_server.repositories;

import org.chc.chcshop_server.models.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, String> {
    // lấy danh sách sản phẩm mới (trạng thái đang bán)
    Page<Product> getAllByPublishedIsTrueOrderByCreatedAtDesc(Pageable pageable);

    // lấy danh sách sản phẩm được xem nhiều (trạng thái đang bán)
    Page<Product> getAllByPublishedIsTrueOrderByViewDesc(Pageable pageable);

    // lấy danh sách sản phẩm theo danh mục
    Page<Product> getAllByCategoryCategoryId(Pageable pageable, String categoryId);

    int countAllByBrand_BrandId(String brandId);
    int countAllBySupplier_SupplierId(String supplierId);
    int countAllByCategory_CategoryId(String categoryId);
}
