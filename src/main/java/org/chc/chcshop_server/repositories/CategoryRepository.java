package org.chc.chcshop_server.repositories;

import org.chc.chcshop_server.models.Category;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CategoryRepository extends MongoRepository<Category, String> {
}
