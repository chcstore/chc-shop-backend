package org.chc.chcshop_server.repositories;

import org.chc.chcshop_server.models.ProductAttribute;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ProductAttributeRepository extends MongoRepository<ProductAttribute, String> {
    List<ProductAttribute> findAllByProduct_ProductId(String productId, Pageable pageable);
    List<ProductAttribute> findAllByProduct_ProductId(String productId);
    ProductAttribute findByProductProductIdAndProductAttributeId(String productId, String productAttributeId);
    long countAllByProduct_ProductId(String productId);
}
