package org.chc.chcshop_server.repositories;

import org.chc.chcshop_server.models.Supplier;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SupplierRepository extends MongoRepository<Supplier, String> {
    List<Supplier> findAllBy(TextCriteria textCriteria);
}
