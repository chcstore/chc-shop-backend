package org.chc.chcshop_server.jwt;

import io.jsonwebtoken.Jwts;
import org.chc.chcshop_server.exceptions.ErrorException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class JWTAuthenticationService {

    @Value("${jwt.auth.secret}")
    private String SECRET;

    @Value("${jwt.auth.prefix-token}")
    private String PREFIX_TOKEN;

    @Value("${jwt.auth.header}")
    private String HEADER;

    public void addAuthentication(HttpServletResponse response, String token) throws IOException {
        response.addHeader(HEADER, String.format("%s %s", PREFIX_TOKEN, token));
        response.addHeader("access-control-expose-headers", "Authorization");
        response.setContentType("application/json;charset=UTF-8");
    }

    public String getInternalToken(HttpServletRequest request) {
        try {
            String token = request.getHeader(HEADER);
            if(token.length() > 0){
                token = token.replace(PREFIX_TOKEN,"").replace(" ","");
            }

            return token;
        } catch (Exception e) {
            throw new ErrorException("Vui lòng đăng nhập");
        }
    }

    public String getUserNameByAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER);
        if (token != null) {
            return Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token.replace(PREFIX_TOKEN, ""))
                    .getBody()
                    .getSubject();
        }
        return null;
    }
}
