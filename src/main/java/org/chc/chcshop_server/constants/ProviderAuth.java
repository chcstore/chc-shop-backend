package org.chc.chcshop_server.constants;

public class ProviderAuth {
    public static final String EMAIL = "email";
    public static final String GOOGLE = "google";
    public static final String FACEBOOK = "facebook";
}
