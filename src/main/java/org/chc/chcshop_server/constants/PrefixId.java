package org.chc.chcshop_server.constants;

public class PrefixId {
    public static final String USER = "user-";
    public static final String BRAND = "brand-";
    public static final String CATEGORY = "category-";
    public static final String SUPPLIER = "supplier-";
    public static final String PRODUCT = "product-";
    public static final String PRODUCT_ATTRIBUTE = "product-attribute-";
    public static final String CITY = "city-";
    public static final String DISTRICT = "district-";
    public static final String WARD = "ward-";
    public static final String ADDRESS_BOOK = "address-book-";
}
