package org.chc.chcshop_server.constants;

public class SessionAttributeConst {
    public static final String CART_SESSION = "CART_SESSION";
    public static final String ORDER_SESSION = "ORDER_SESSION";
}
