package org.chc.chcshop_server.constants;

public class OrderStatus {
    public static final int DAT_HANG = 1;
    public static final int TIEP_NHAN = 2;
    public static final int DONG_GOI = 3;
    public static final int VAN_CHUYEN = 4;
    public static final int THANH_CONG = 5;
}
