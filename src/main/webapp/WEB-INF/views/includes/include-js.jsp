<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- Core -->
<script src="<c:url value="/assets/js/jquery-3.5.1.slim.min.js"/>"></script>
<script src="<c:url value="/assets/js/popper.min.js"/>"></script>
<script src="<c:url value="/assets/bootstrap/js/bootstrap.min.js"/>"></script>

<!-- Theme JS -->
<script src="<c:url value="/assets/argon-dashboard/assets/js/argon.min.js"/>"></script>