<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Favicon -->
<link rel="icon" href="<c:url value="/assets/images/favicon.png"/>" type="image/png">

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet">

<!-- Icons -->
<link href="<c:url value="/assets/fontawesome-free-5.13.0/css/all.min.css"/>" rel="stylesheet">

<!-- Theme CSS -->
<link type="text/css" href="<c:url value="/assets/argon-dashboard/assets/css/argon.min.css"/>"
      rel="stylesheet">