<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/admin">
                CHC Shop Admin
            </a>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" href="${pageContext.request.contextPath}/admin/dashboard">
                            <i class="fas fa-tachometer-alt text-primary"></i>
                            <span class="nav-link-text">Tổng quan</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${pageContext.request.contextPath}/admin/orders">
                            <i class="fas fa-shopping-cart text-orange"></i>
                            <span class="nav-link-text">Quản lý đơn hàng</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${pageContext.request.contextPath}/admin/products">
                            <i class="fas fa-box text-primary"></i>
                            <span class="nav-link-text">Quản lý sản phẩm</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${pageContext.request.contextPath}/admin/categories">
                            <i class="fas fa-boxes text-yellow"></i>
                            <span class="nav-link-text">Quản lý danh mục</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${pageContext.request.contextPath}/admin/brands">
                            <i class="fas fa-copyright text-info"></i>
                            <span class="nav-link-text">Quản lý thương hiệu</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${pageContext.request.contextPath}/admin/suppliers">
                            <i class="fas fa-house-user text-default"></i>
                            <span class="nav-link-text">Quản lý nhà cung cấp</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${pageContext.request.contextPath}/admin/users">
                            <i class="fas fa-users text-pink"></i>
                            <span class="nav-link-text">Quản lý người dùng</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>