<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<nav class="navbar navbar-expand-lg navbar-dark bg-default">
    <div class="container-fluid">
        <a class="navbar-brand mx-3" href="${pageContext.request.contextPath}/">
            <i class="fas fa-headphones-alt"></i>
            CHC SHOP
        </a>

        <!-- Search form -->
        <form method="get" class="navbar-search navbar-search-light form-inline ml-2">
            <div class="form-group mb-0">
                <div class="input-group input-group-alternative input-group-merge" style="width: 28vw">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fas fa-search"></i>
                        </span>
                    </div>
                    <input class="form-control" placeholder="Nhập từ khóa cần tìm"
                           name="q" type="text" value="${q}">
                </div>
            </div>
        </form>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-default"
                aria-controls="navbar-default" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-default">
            <div class="navbar-collapse-header">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <i class="fas fa-headphones-alt"></i>
                        CHC SHOP
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse"
                                data-target="#navbar-default" aria-controls="navbar-default" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>

            <ul class="navbar-nav ml-lg-auto align-items-center">
                <li class="nav-item dropdown">
                    <button type="button" class="btn btn-outline-secondary"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="far fa-user"></i>
                        <span class="mb-0 text-sm font-weight-bold">John Snow</span>
                    </button>

                    <div class="dropdown-menu  dropdown-menu-right ">
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Đăng nhập để tiếp tục</h6>
                        </div>
                        <a class="dropdown-item" href="${pageContext.request.contextPath}/login">
                            <i class="fas fa-sign-in-alt"></i>
                            <span>Đăng nhập</span>
                        </a>
                        <a class="dropdown-item" href="${pageContext.request.contextPath}/register">
                            <i class="fas fa-user-edit"></i>
                            <span>Đăng ký</span>
                        </a>
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Chào bạn !</h6>
                        </div>
                        <a class="dropdown-item" href="${pageContext.request.contextPath}/admin">
                            <i class="fas fa-user-shield"></i>
                            <span>Trang quản lý</span>
                        </a>
                        <a class="dropdown-item" href="${pageContext.request.contextPath}/admin">
                            <i class="fas fa-shopping-bag"></i>
                            <span>Đơn hàng của tôi</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#!" class="dropdown-item">
                            <i class="fas fa-sign-out-alt"></i>
                            <span>Đăng xuất</span>
                        </a>
                    </div>
                </li>

                <li class="nav-item">
                    <a href="${pageContext.request.contextPath}/cart">
                        <button class="btn btn-outline-danger">
                            <i class="fas fa-shopping-cart"></i>
                            Giỏ hàng
                            <span class="badge badge-light">4</span>
                        </button>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>