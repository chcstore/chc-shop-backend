<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!doctype html>
<html lang="en">
<head>
    <jsp:include page="../includes/include-assets.jsp"/>
    <title>Đăng nhập</title>
</head>
<body class="bg-default">
<jsp:include page="../shareds/nav-bar.jsp"/>

<div class="main-content">
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
        <div class="container">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-xl-5 col-lg-6 col-md-8 px-5">
                        <h1 class="text-white">Chào bạn !</h1>
                        <p class="text-lead text-white">Đăng nhập để tiếp tục nhé !</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-8">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-7">
                <div class="card bg-secondary border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-left text-muted mb-4">
                            <h3>Đăng nhập</h3>
                        </div>
                        <c:if test="${ message.length() > 0 }">
                            <div class="text-center py-2">
                                <span class="text-danger"><c:out value="${ message }"/></span>
                            </div>
                        </c:if>
                        <form:form action="login" method="post" modelAttribute="loginRequest">
                            <div class="form-group mb-3">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="far fa-envelope"></i>
                                                                </span>
                                    </div>
                                    <form:input path="email" class="form-control" placeholder="Email" type="email"/>
                                </div>
                            </div>
                            <div class="form-group focused">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="fas fa-unlock"></i>
                                                                </span>
                                    </div>
                                    <form:password path="password" class="form-control" placeholder="Mật khẩu"/>
                                </div>
                            </div>
                            <div class="custom-control custom-control-alternative custom-checkbox">
                                <form:checkbox path="remember" class="custom-control-input" id="remember"/>
                                <label class="custom-control-label"
                                       for="remember"><span>Ghi nhớ đăng nhập</span></label>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary my-4">Đăng nhập</button>
                            </div>
                        </form:form>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-6">
                        <a href="${pageContext.request.contextPath}/forgot-password" class="text-light">
                            <small>Quên mật khẩu ?</small>
                        </a>
                    </div>
                    <div class="col-6 text-right">
                        <a href="${pageContext.request.contextPath}/register" class="text-light">
                            <small>Đăng ký thành viên mới</small>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../includes/include-js.jsp"/>
</body>
</html>
