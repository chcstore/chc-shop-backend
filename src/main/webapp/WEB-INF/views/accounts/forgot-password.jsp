<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!doctype html>
<html lang="en">
<head>
    <jsp:include page="../includes/include-assets.jsp"/>
    <title>Quên mật khẩu</title>
</head>
<body class="bg-default">
<jsp:include page="../shareds/nav-bar.jsp"/>

<div class="main-content">
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-8">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-7">
                <div class="card bg-secondary border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-left text-muted mb-4">
                            <h3>Quên mật khẩu ?</h3>
                        </div>
                        <form role="form">
                            <div class="form-group">
                                <div class="input-group input-group-merge input-group-alternative mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-envelope"></i>
                                        </span>
                                    </div>
                                    <input class="form-control" placeholder="Email" type="email">
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="button" class="btn btn-primary mt-1">Đặt lại mật khẩu</button>
                            </div>
                            <div class="text-left pt-4">
                                <a href="${pageContext.request.contextPath}/login">
                                    <i class="fas fa-chevron-left"></i>
                                    Đăng nhập
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../includes/include-js.jsp"/>
</body>
</html>
