<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">
<head>
    <jsp:include page="../../includes/include-assets.jsp"/>
    <title>Quản lý thương hiệu</title>
</head>
<body>
<jsp:include page="../../shareds/side-bar-admin.jsp"/>

<div class="main-content" id="panel">
    <jsp:include page="../../shareds/nav-bar-admin.jsp"/>
    <h1>Quản lý thương hiệu</h1>
</div>

<jsp:include page="../../includes/include-js.jsp"/>
</body>
</html>
