<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!doctype html>
<html lang="en">
<head>
    <jsp:include page="../../includes/include-assets.jsp"/>
    <title>Quản lý nhà cung cấp</title>
</head>
<body>
<jsp:include page="../../shareds/side-bar-admin.jsp"/>

<div class="main-content" id="panel">
    <div class="header bg-gradient-default pb-6">
        <jsp:include page="../../shareds/nav-bar-admin.jsp"/>

        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item">
                                    <a href="${pageContext.request.contextPath}/admin"><i class="fas fa-home"></i></a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="${pageContext.request.contextPath}/admin/suppliers">Quản lý nhà cung
                                        cấp</a>
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!-- Card stats -->
                <div class="row">
                    <div class="col-xl-3 col-md-6">
                        <div class="card card-stats">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Total traffic</h5>
                                        <span class="h2 font-weight-bold mb-0">350,897</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                            <i class="ni ni-active-40"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-sm">
                                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                    <span class="text-nowrap">Since last month</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card card-stats">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">New users</h5>
                                        <span class="h2 font-weight-bold mb-0">2,356</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                            <i class="ni ni-chart-pie-35"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-sm">
                                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                    <span class="text-nowrap">Since last month</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card card-stats">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Sales</h5>
                                        <span class="h2 font-weight-bold mb-0">924</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                            <i class="ni ni-money-coins"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-sm">
                                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                    <span class="text-nowrap">Since last month</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card card-stats">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Performance</h5>
                                        <span class="h2 font-weight-bold mb-0">49,65%</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                                            <i class="ni ni-chart-bar-32"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-sm">
                                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                    <span class="text-nowrap">Since last month</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <a class="text-danger" href="${pageContext.request.contextPath}/admin/supplier">
                                    <button type="button" class="btn btn-sm btn-primary">
                                        <i class="fas fa-plus"></i>
                                        Thêm
                                    </button>
                                </a>
                            </div>
                            <div class="col-4 text-right">
                                <!-- Search form -->
                                <form method="get" action="${pageContext.request.contextPath}/admin/suppliers" class="navbar-search navbar-search-light form-inline">
                                    <div class="form-group mb-0">
                                        <div class="input-group input-group-alternative input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-search"></i></span>
                                            </div>
                                            <input class="form-control" placeholder="Nhập từ khóa cần tìm"
                                                   name="q" type="text" value="${q}">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Mã nhà cung cấp</th>
                                <th scope="col">Tên nhà cung cấp</th>
                                <th scope="col">Tên người liên hệ</th>
                                <th scope="col">Số điện thoại</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${listSupplier}" var="supplier">
                                    <tr>
                                        <th scope="row"> <c:out value="${supplier.supplierId}"/> </th>
                                        <td><c:out value="${supplier.supplierName}"/></td>
                                        <td><c:out value="${supplier.contactPerson}"/></td>
                                        <td><c:out value="${supplier.phone}"/></td>
                                        <td class="text-right">
                                            <a href="${pageContext.request.contextPath}/admin/supplier?id=${supplier.supplierId}">
                                                <button class="btn btn-icon btn-sm btn-outline-primary" type="button">
                                                <span class="btn-inner--icon px-1">
                                                    <i class="fas fa-user-edit"></i>
                                                </span>
                                                </button>
                                            </a>
                                            <button class="btn btn-icon btn-sm btn-danger" type="button">
                                                <span class="btn-inner--icon px-1">
                                                    <i class="far fa-trash-alt"></i>
                                                </span>
                                            </button>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer -->
        <jsp:include page="../../shareds/footer-admin.jsp"/>
    </div>
</div>

<jsp:include page="../../includes/include-js.jsp"/>
</body>
</html>
