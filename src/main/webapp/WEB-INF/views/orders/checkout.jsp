<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">
<head>
    <jsp:include page="../includes/include-assets.jsp"/>
    <title>Đặt hàng</title>
</head>
<body>
<jsp:include page="../shareds/nav-bar.jsp"/>

<div class="container">
    <div class="row pt-3">
        <div class="col-12 mb-3">
            <h3 class="d-inline">Giỏ hàng</h3>
            <small>(0 sản phẩm)</small>
        </div>
        <div class="col-9">
            <div class="card">
                <div class="card-body px-2 py-2">
                    <div class="row">
                        <div class="col-3">
                            <img class="img-thumbnail" src="<c:url value="/assets/images/faddf74eeba033d8c4a6e40bbf50d9a0.jpg"/>" alt="product image">
                        </div>

                        <div class="col-7">
                            <strong class="d-block">Tên sản phẩm</strong>
                            <small class="mb-3 mt-1 d-block">Màu sắc</small>
                            <p class="mb-1">100.000 đ</p>
                            <small>
                                <strike>110.000 đ</strike> |
                                <strong>10 %</strong>
                            </small>
                        </div>

                        <div class="col-2 pt-2 pr-4">
                            <form>
                                <div class="form-group">
                                    <input class="form-control" type="number" value="1" step="1" min="0">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h5 class="d-inline">Tạm tính</h5>
                            <small class="float-right">100.000 đ</small>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-12">
                            <h5 class="d-inline">Thành tiền</h5>
                            <small class="float-right h4 text-danger">100.000 đ</small>
                        </div>
                        <div class="col-12">
                            <small class="text-right py-0 mb-0 d-block">(Đã bao gồm VAT nếu có)</small>
                        </div>
                    </div>
                </div>
            </div>
            <button type="button" class="btn btn-block btn-danger my-1">TIẾN HÀNH ĐẶT HÀNG</button>
        </div>
    </div>
</div>

<jsp:include page="../includes/include-js.jsp"/>
</body>
</html>
