# CHC WWW Backend
### Cài đặt Project Lombok Plugin
1. Eclipse
- Chạy file lombok.jar trong thư mục plugin
- Check những tuỳ chọn cho Eclipse
- Nhấn Install/Update
- Xem chi tiết tại https://projectlombok.org/setup/eclipse
2. Intellij IDEA
- Vào Settings > Plugin
- Tìm Lombok
- Nhấn Install > Restart IDEA
- Vào Settings > Build, Execution, Deployment > Compiler > Annotation Processors
- Check vào Enable annotation processing và Obtain processors from project classpath
- Xem chi tiết tại https://projectlombok.org/setup/intellij